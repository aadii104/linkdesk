const mongoose = require('mongoose')
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/linkDesk')

const db = mongoose.connection

db.once('error', () => { console.log('Mongodb error') })
db.once('open', () => { console.log('Mongodb connected') })
const bcrypt = require('bcrypt-nodejs')
const UserSchema = mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: String,
  email: String,
  created_at: { type: Date, default: Date.now }
})

UserSchema.pre('save', function (next) {
  var user = this
  bcrypt.hash(user.password, null, null, function (err, hash){
    if (err) return next(err)
    user.password = hash
    next()
  })
})

module.exports = mongoose.model('User', UserSchema)
