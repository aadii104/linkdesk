import Vue from 'vue'
import Router from 'vue-router'
import Hello from '../components/Hello.vue'
import Signup from '../components/Signup.vue'
import Login from '../components/Login.vue'
Vue.use(Router)

const routes = [
  { path: '/login', component: Login },
  { path: '/signup', component: Signup },
  { path: '/', component: Hello }
]

const router = new Router({
  mode: 'history',
  routes: routes
})

export default router
