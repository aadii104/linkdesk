import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

const state = {
  user: {
    username: '',
    email: '',
    password: ''
  },
  userList: [],
  showAlter: false,
  updateArr: [],
  updateUsers: {
    updateUserStr: '',
    updatePasswordStr: ''
  },
  loginShow: false
}

// console.log(state.user)
const actions = {
  register: ({commit}) => {
    console.log('commit')
    commit('register')
  }
}

const mutations = {
  register (state) {
    console.log('mutation state')
    console.log(state)
    let params = {
      email: state.user.email,
      username: state.user.username,
      password: state.user.password
    }
    Axios({
      method: 'post',
      data: params,
      url: 'http://localhost:3000/api/users'
    }).then((response) => {
      console.log('response' + response)
    }).catch((err) => {
      console.log(err)
    })
  }
}

const getters = {
  registerUP (state) {
    console.log('state.user')
    console.log(state.user)
    return state.user
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
export default store
